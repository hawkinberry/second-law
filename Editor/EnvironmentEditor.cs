using UnityEngine;
using UnityEditor;

namespace Consequences.Editors
{
    [CustomEditor(typeof(Environments.Environment))]
    public class EnvironmentEditor : Editor
    {
        const int BUTTON_HEIGHT = 30;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            Environments.Environment environment = (Environments.Environment)target;

            if (GUILayout.Button("Advance", GUILayout.Height(BUTTON_HEIGHT)))
            {
                environment.AdvanceTrack();
            }
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Consequences.Environments
{
    public class TransitionDetector : MonoBehaviour
    {
        public static SceneTransitionEventHandler OnPlayerTransit;

        public bool m_Debug;

        private Environment m_parent;
        public Environment targetScene;

        private void Start()
        {
            m_parent = GetComponentInParent<Environment>();
        }

        public void OnTriggerEnter2D(Collider2D collision)
        {
            if (Player.IsPlayer(collision))
            {
                if (m_Debug) { Debug.Log("Player moving from " + m_parent.name + " to " + targetScene.name); }

                OnPlayerTransit?.Invoke(targetScene);
            }
        }

        public delegate void SceneTransitionEventHandler(Environment transitionTo);
    }
}
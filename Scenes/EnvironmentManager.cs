using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Consequences.Environments
{

    public class EnvironmentManager : MonoBehaviour
    {
        // Make singleton
        private static EnvironmentManager m_instance;

        public Transform m_Camera;

        private static Environment s_currentEnvironment;
        public Environment startEnvironment;

        public Player m_Player;

        private void Awake()
        {
            if (!m_instance)
            {
                m_instance = this;
            }
        }

        private void Start()
        {
            MakeTransition(startEnvironment);
        }

        public static EnvironmentManager GetInstance()
        {
            return m_instance;
        }

        public static Environment GetCurrentEnvironment()
        {
            return s_currentEnvironment;
        }

        private void OnEnable()
        {
            TransitionDetector.OnPlayerTransit += MakeTransition;
        }

        private void OnDisable()
        {
            TransitionDetector.OnPlayerTransit -= MakeTransition;
        }

        private void MakeTransition(Environment newEnvironment)
        {
            SetCurrent(newEnvironment);
        }

        public static void SetCurrent(Environment environment)
        {
            int preserveMusicTrack = 0;

            // Activate new environment
            environment.gameObject.SetActive(true);

            // Will not run on first load
            if (s_currentEnvironment)
            {
                // Deactivate old environment
                preserveMusicTrack = s_currentEnvironment.Deactivate();
                s_currentEnvironment.gameObject.SetActive(false); // this may be more performant, but we need to keep elements awake and listening

                // Set player position to spawn position
                GetInstance().m_Player.Spawn(environment.entryPosition.position);
            }

            s_currentEnvironment = environment;

            environment.Activate(preserveMusicTrack);

            Transform reference = GetInstance().m_Camera;

            // Set camera to center of new environment
            reference.position = new Vector3(environment.transform.position.x, 
                                             environment.transform.position.y, 
                                             reference.position.z);
        }

        
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Consequences.Environments
{
    using Utility.Audio;

    public class Environment : MonoBehaviour
    {
        public static SeverityAdvanceHandler OnSeverityIncrease;

        public const int USE_CURRENT_TRACK = -1;

        public Transform entryPosition;

        public AudioClip m_footStep;

        public List<AudioClip> m_ambienceTracks;
        public List<AudioClip> m_musicTracks;

        //! Current track index
        //! Automatically guards against incrementing above limit
        private int m_currentAmbienceIdx;
        private int currentAmbienceIdx
        {
            get { return m_currentAmbienceIdx; }

            set
            {
                m_currentAmbienceIdx = Mathf.Clamp(value, 0, m_ambienceTracks.Count);
            }
        }

        //! Current music index
        //! Automatically guards against incrementing above limit
        private int m_currentMusicIdx;
        private int currentMusicIdx
        {
            get { return m_currentMusicIdx; }

            set
            {
                m_currentMusicIdx = Mathf.Clamp(value, 0, m_musicTracks.Count);
            }
        }

        private void OnEnable()
        {
            Environment.OnSeverityIncrease += AdvanceTrack;
        }

        private void OnDisable()
        {
            Environment.OnSeverityIncrease -= AdvanceTrack;
        }

        // Start is called before the first frame update
        void Start()
        {
            //Activate self if designated the current environment
            this.gameObject.SetActive(EnvironmentManager.GetCurrentEnvironment() == this);
        }

        public void Activate(int overrideTrackIdx = USE_CURRENT_TRACK)
        {
            int trackIdx = (overrideTrackIdx > USE_CURRENT_TRACK) ? overrideTrackIdx : currentAmbienceIdx;

            PlayAmbience(currentAmbienceIdx);
            PlayMusic(trackIdx, resume: true);
        }

        public int Deactivate()
        {
            StopAmbience();
            StopMusic(resume: true);

            return currentMusicIdx;
        }

        void PlayAmbience(int idx = 0)
        {
            if (idx < m_ambienceTracks.Count)
            {
                AudioManager.PlaySfx(m_ambienceTracks[idx], true);

                currentAmbienceIdx = idx;
            }
            else
            {
                Debug.Log("Environment " + gameObject.name + " cannot find ambience track " + idx);
            }
        }

        void StopAmbience()
        {
            AudioManager.StopSfx();
        }

        void PlayMusic(int idx = 0, bool resume = false)
        {
            if (idx < m_musicTracks.Count)
            {
                AudioManager.PlayMusic(m_musicTracks[idx], loop: true, resume: resume);

                currentMusicIdx = idx;
            }
            else
            {
                Debug.Log("Environment " + gameObject.name + " cannot find music track " + idx);
            }
        }

        void StopMusic(bool resume = false)
        {
            AudioManager.StopMusic(resume: resume);
        }

        public void AdvanceTrack()
        {
            // The index will guard against over-incrementing
            // But we still prefer to not replay the track if told to advance
            if (currentAmbienceIdx < (m_ambienceTracks.Count - 1))
            {
                StopAmbience();
                PlayAmbience(++currentAmbienceIdx);
            }

            // The index will guard against over-incrementing
            // But we still prefer to not replay the track if told to advance
            if (currentMusicIdx < (m_musicTracks.Count - 1))
            {
                StopMusic(resume: true);
                PlayMusic(++currentMusicIdx);
            }
        }

        public delegate void SeverityAdvanceHandler();
    }
}
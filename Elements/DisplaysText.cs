using UnityEngine;
using TMPro;

public class DisplaysText : MonoBehaviour
{
    // Used to indicate the interaction proximity
    // NEAR will display text above element
    // DISTANT will not
    public enum ELEMENT_PROXIMITY
    {
        NEAR,
        DISTANT
    };

    [Header("Dialogue")]
    public bool m_Debug = false;
    [SerializeField] private string m_defaultText;
    private Canvas m_display;
    private bool m_IsDisplaying = false;

    public ELEMENT_PROXIMITY type;

    public virtual string displayText
    {
        get { return m_defaultText; }
    }

    public bool isDisplaying
    {
        get { return m_IsDisplaying; }

        private set
        {
            m_IsDisplaying = value;
            if (m_display)
            {
                m_display.enabled = value; // I don't know if this is a good idea
            }
        }
    }

    protected virtual void Awake()
    {
        m_display = transform.GetComponentInChildren<Canvas>();

        // Init display text
        SetDisplayText();
        this.isDisplaying = false;
    }

    private void SetDisplayText(string message = null)
    {
        if (m_display)
        {
            TextMeshProUGUI text = m_display.GetComponentInChildren<TextMeshProUGUI>();

            if (message != null)
            {
                text.text = message;
            }
            else
            {
                text.text = displayText;
            }
        }
    }

    // Activate interaction display text
    public void DisplayText(string overrideText = null)
    {
        string textToDisplay = displayText;
        if (overrideText != null)
        {
            textToDisplay = overrideText;
        }

        SetDisplayText(textToDisplay);

        // Show display if it is not already showing
        if (!this.isDisplaying)
        {
            if (m_Debug) { Debug.Log(textToDisplay); }

            this.isDisplaying = true; // will also enable canvas display
        }
    }

    // Deactivate interaction display text
    public void HideDisplay()
    {
        if (this.isDisplaying)
        {
            this.isDisplaying = false; // will also disable canvas display

            // Reset display text
            SetDisplayText();
        }
    }
}

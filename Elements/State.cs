using System.Collections.Generic;
using UnityEngine;

namespace Consequences.Elements
{
    [CreateAssetMenu(menuName = "State")]
    public class State : ScriptableObject
    {
        public static StateChangeEvent OnStateChange;

        #region enums
        public enum ENTRY_TYPE
        {
            DEFAULT,
            BLOCK,
        }

        public enum TRIGGER_TYPE
        {
            INTERACT,
            CHAIN,
            FINAL = 99
        };

        public enum ON_ENTRY
        {
            NONE, // No extra result
            AWAKEN, // On trigger, awaken parent
        }
        #endregion

        // Text to evoke when this state is triggered
        [SerializeField] private string m_eventText;
        public string eventText
        {
            get { return m_eventText; }
            private set { m_eventText = value; }
        }

        // Text that explains the event
        [SerializeField] private string m_explanation;
        public string explainText
        {
            get { return m_explanation; }
            private set { m_explanation = value; }
        }

        public float entropyValue;

        // Trigger settings
        [Header("Entry")]
        public ENTRY_TYPE entryType;
        public float entryDelaySeconds;
        // How to handle entry
        public ON_ENTRY entryTriggerResult;
        // Play on Entry
        public AnimationClip animationOnEntry;
        public AudioClip audioOnEntry;
        public Sprite spriteOnEntry;

        [Header("Exit")]
        public float exitDelaySeconds;
        // How to transition to exit
        public TRIGGER_TYPE exitTriggerType;
        

        

        [HideInInspector] public Element m_Parent;

        // List of states to be triggered when this state is triggered
        public List<State> autoTriggerOnExit;

        public List<State> autoDisableOnExit;

        public State()
        {
            entropyValue = Unstable.DEFAULT_ENTROPY;
        }

        public State(Element aParent, string text)
        {
            m_Parent = aParent;
            eventText = text;
        }

        // Used when interacted with
        // Returns true if activation resulted in a trigger
        // ! We get here from Element.Interact OR State.Trigger
        public bool Activate(bool bypass = false)
        {
            if (exitTriggerType == TRIGGER_TYPE.INTERACT || bypass)
            {
                if (m_Parent.m_Debug) { Debug.Log(eventText); }

                /*switch (entryTriggerResult)
                {
                    case ON_ENTRY.AWAKEN:
                        m_Parent.SetAwake();
                        break;
                    default:
                        break;
                }*/

                // Trigger all connected states
                foreach (State s in autoTriggerOnExit)
                {
                    s.Trigger();
                }

                // Disable connected elements
                foreach (State s in autoDisableOnExit)
                {
                    s.m_Parent.Deactivate();
                }

                return true;
            }

            return false;
        }

        // This state has been triggered
        private void Trigger()
        {
            if (entryTriggerResult == ON_ENTRY.AWAKEN)
            {
                m_Parent.SetAwake(); // Probably not good design to let the child decide the parent response
            }

            // Auto trigger if this is a chain
            if (exitTriggerType == TRIGGER_TYPE.CHAIN || exitTriggerType == TRIGGER_TYPE.FINAL)
            {
                this.Activate(true);
                m_Parent.AdvanceState(); // Probably not good design to let the child decide the parent response
            }
            // Wait for interaction to activate
            else if (exitTriggerType == TRIGGER_TYPE.INTERACT)
            {
               // Nothing extra
            }
        }

        public delegate void StateChangeEvent(State newState, string altText = null);
    }
}
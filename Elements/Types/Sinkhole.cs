using UnityEngine;

namespace Consequences.Elements
{
    using Utility.Menus;

    public class Sinkhole : Element
    {
        public float m_EndDelay = 0.5f;

        protected override void DoExit()
        {
            base.DoExit();

            // TMP - Really should do a check to make sure it's the final state
            // But at this point I know there is only one
            Debug.Log("Ending the game!");
            Invoke("GameOver", m_EndDelay);
        }

        private void GameOver()
        {
            Utility.Executive.OnGameOver?.Invoke();
        }
    }
}
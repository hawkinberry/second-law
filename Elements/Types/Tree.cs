using UnityEngine;

namespace Consequences.Elements
{
    public class Tree : Element
    {
        public enum SECONDARY_SPRITE
        {
            NOTHING,
            DISABLE,
            ENABLE
        };

        public SpriteRenderer m_SecondarySprite;
        public SECONDARY_SPRITE m_DisableSecondary;

        public Utility.CameraShake m_Camera;

        protected override void DoExit()
        {
            base.DoExit();

            Debug.Log("Exiting from state " + currentStateIndex);

            if (currentStateIndex == 2) // HACK
            {
                Debug.Log("Executing tree");

                // TMP - Really should do a check to make sure it's the final state
                // But at this point I know there is only one

                MakeStuffHappen();
            }
        }

        private void MakeStuffHappen()
        {
            m_Animator.SetBool("m_Fall", true);

            AnimationClip[] clips = m_Animator.runtimeAnimatorController.animationClips;
            float delay = 0.0f;
            foreach (AnimationClip a in clips)
            {
                if (a.name.Contains("Fall"))
                {
                    delay = a.length;
                }
            }

            Invoke("HandleSecondarySprite", delay);
        }

        private void HandleSecondarySprite()
        {
            StartCoroutine(m_Camera.Shake(0.2f, .4f));

            switch (m_DisableSecondary)
            {
                case SECONDARY_SPRITE.DISABLE:
                    m_SecondarySprite.enabled = false;
                    break;
                case SECONDARY_SPRITE.ENABLE:
                    m_SecondarySprite.enabled = true;
                    break;
                case SECONDARY_SPRITE.NOTHING:
                default:
                    break;
            }
        }
    }
}
using UnityEngine;

namespace Consequences.Elements
{
    public class Stump : Element
    {
        public enum SECONDARY_SPRITE
        {
            NOTHING,
            DISABLE,
            ENABLE
        };

        public SpriteRenderer m_SecondarySprite;
        public SECONDARY_SPRITE m_DisableSecondary;

        protected override void DoExit()
        {
            base.DoExit();

            Debug.Log("Exiting from state " + currentStateIndex);

            if (currentStateIndex == 2) // HACK
            {
                Debug.Log("Executing stump");

                // TMP - Really should do a check to make sure it's the final state
                // But at this point I know there is only one

                HandleSecondarySprite();
            }
        }

        private void HandleSecondarySprite()
        {

            switch (m_DisableSecondary)
            {
                case SECONDARY_SPRITE.DISABLE:
                    m_SecondarySprite.enabled = false;
                    break;
                case SECONDARY_SPRITE.ENABLE:
                    m_SecondarySprite.enabled = true;
                    break;
                case SECONDARY_SPRITE.NOTHING:
                default:
                    break;
            }
        }
    }
}

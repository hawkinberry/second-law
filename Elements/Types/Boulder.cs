using UnityEngine;

namespace Consequences.Elements
{
    public class Boulder : Element
    {
        // Add or extend functionality here
        private SpriteRenderer m_Shadow;

        public Utility.CameraShake m_Camera;

        protected override void Start()
        {
            base.Start();

            // HACK
            m_Shadow = transform.Find("Sprite").transform.Find("Shadow").GetComponent<SpriteRenderer>();
        }

        protected override void DoExit()
        {
            base.DoExit();

            Debug.Log("Executing fall");

            // TMP - Really should do a check to make sure it's the final state
            // But at this point I know there is only one

            FallBoulder();
        }

        private void FallBoulder()
        {
            // Increase severity
            Environments.Environment.OnSeverityIncrease?.Invoke();

            m_Shadow.enabled = false;

            //m_Shadow.sortingLayerName = "Foreground";
            m_Animator.SetBool("m_Fall", true);

            StartCoroutine(m_Camera.Shake());

            AnimationClip[] clips = m_Animator.runtimeAnimatorController.animationClips;
            float delay = 0.0f;
            foreach (AnimationClip a in clips)
            {
                if (a.name.Contains("Fall"))
                {
                    delay = a.length;
                }
            }

            Invoke("Disable", delay);
        }

        private void Disable()
        {
            gameObject.SetActive(false);
        }
    }
}
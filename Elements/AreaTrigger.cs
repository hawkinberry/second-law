using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Consequences.Elements
{
    public interface IHandleArea
    {
        public abstract void AreaEntered();
        public abstract void AreaExited();
    }

    public class AreaTrigger : MonoBehaviour
    {
        public static ElementEnterEvent OnAreaEnter;
        public static ElementEnterEvent OnAreaExit;

        //private Element m_Parent;
        private IHandleArea m_Parent;

        private void Awake()
        {
            //m_Parent = transform.GetComponentInParent<Element>();
            m_Parent = (IHandleArea)transform.GetComponentInParent(typeof(IHandleArea));

            if (m_Parent == null)
            {
                Debug.Log("Could not find IHandleArea");
            }
        }

        public void OnTriggerEnter2D(Collider2D collision)
        {
            if (Player.IsPlayer(collision))
            {
                // engage element
                //m_Parent.Disengage(); // let player handle this

                OnAreaEnter?.Invoke(m_Parent);
                m_Parent.AreaEntered();
            }
        }

        public void OnTriggerExit2D(Collider2D collision)
        {
            if (Player.IsPlayer(collision))
            {
                // Disengage from element
                //m_Parent.Disengage(); // let player handle this

                OnAreaExit?.Invoke(m_Parent);
                m_Parent.AreaExited();
            }
        }

        //public delegate void ElementEnterEvent(Element obj);
        public delegate void ElementEnterEvent(IHandleArea obj);
    }
}
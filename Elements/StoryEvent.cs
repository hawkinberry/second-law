using System;
using UnityEngine;
using TMPro;

public class StoryEvent : MonoBehaviour
{
    [HideInInspector] public string Title;
    [HideInInspector] public string Content;

    TextMeshProUGUI titleTM;
    TextMeshProUGUI contentTM;

    private void Awake()
    {
        titleTM = transform.Find("Title").GetComponent<TextMeshProUGUI>();
        contentTM = transform.Find("Context").GetComponent<TextMeshProUGUI>();
    }

    public void SetTitle(string title)
    {
        Title = title;
        if (titleTM)
        {
            titleTM.text = title;
        }
    }

    public void SetContent(string content)
    {
        Content = content;
        if (contentTM)
        {
            contentTM.text = content;
        }
    }

    public bool IsEqual(Tuple<string, string> strings)
    {
        return (this.Title == strings.Item1 && this.Content == strings.Item2);
    }
}

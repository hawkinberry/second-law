using System.Collections.Generic;
using UnityEngine;

namespace Consequences.Elements
{
    using Utility.Audio;

    public class Element : DisplaysText, IHandleArea
    {
        public bool startAwake;
        private bool m_isAwake = true;
        public bool isAwake { get { return m_isAwake; } private set { m_isAwake = value; } }

        private bool isEngaged;
        private bool m_isDeactivated = false;

        protected SpriteRenderer m_Sprite;
        protected Animator m_Animator;
        protected Animation m_Animation;
        protected Collider2D m_collider2D;
        protected Collider2D m_areaTrigger;

        public bool stopAnimationAfterInteraction;

        // Remeber the collider setting to restore
        private bool colliderSetting;

        public List<State> elementStates;

        private int m_currentStateIdx;
        protected int currentStateIndex
        {
            get { return m_currentStateIdx; }
            set
            {
                m_currentStateIdx = Mathf.Clamp(value, 0, elementStates.Count);
            }
        }

        public override string displayText
        {
            get 
            { 
                return (GetCurrentState() ? GetCurrentState().eventText : base.displayText); 
            }
        }

        protected override void Awake()
        {
            base.Awake();

            isEngaged = false;
            m_currentStateIdx = 0;

            // Set ownership of all states to self
            foreach (State s in elementStates)
            {
                s.m_Parent = this;
            }
            
        }

        protected virtual void Start()
        {
            // Register with game monitor
            foreach (State s in elementStates)
            {
                //s.m_Parent = this; // do this here or in Awake ?
                //Unstable.OnRegister?.Invoke(s.entropyValue);
                Unstable.Register(s.entropyValue);
            }

            // Pass param "true" to search in disabled objects
            m_Sprite = transform.GetComponentInChildren<SpriteRenderer>(true);
            m_Animator = transform.GetComponentInChildren<Animator>(true);
            m_Animation = transform.GetComponentInChildren<Animation>(true);
            m_collider2D = transform.GetComponent<Collider2D>();
            m_areaTrigger = transform.GetComponentInChildren<Collider2D>(); // NOTE: This may not exist for non-interactables

            if (!startAwake)
            {
                this.Sleep();
            }
        }

        public virtual void AreaEntered()
        {
            // No action
        }

        public virtual void AreaExited()
        {
            // No action
        }

        // Block any future state transitions
        public void Deactivate()
        {
            if (m_Debug) { Debug.Log("Deactivating " + this.name); }
            m_isDeactivated = true;
            Sleep();
        }

        /**
        public static Element GetElement(Collider2D collision)
        {
            Element obj = null;
            // Check in transform
            Transform parent = collision.transform;
            obj = parent.GetComponent<Element>();

            if (obj == null)
            {
                // Check in parent
                obj = parent.GetComponentInParent<Element>();
            }

            // Will return null if component does not exist
            return obj;
        }
        **/

        // Will return null if there is an internal count error
        public State GetCurrentState()
        {
            // Return current list index if we haven't incremented m_currentState beyond bounds
            return ((m_currentStateIdx < elementStates.Count) ? elementStates[m_currentStateIdx] : null);
        }

        // Hide visible / physical portions
        public void Sleep()
        {
            if (m_Debug) { Debug.Log("Sleeping " + this.name); }
            // Disable visuals
            m_Sprite.gameObject.SetActive(false);

            // Temporarily disable the collider
            colliderSetting = m_collider2D.isTrigger; // remember the original setting

            this.Disable();

            isAwake = false;
        }

        // Activate visible / physical portions
        public void SetAwake()
        {
            if (m_isDeactivated)
            {
                return;
            }

            // Enable sprite
            if (!m_Sprite)
            {
                Debug.Log("Oops! " + name + " did not initialize!");
            }
            else
            {
                m_Sprite.gameObject.SetActive(true);
            }

            // Restore original collider setting
            if (m_collider2D)
            {
                m_collider2D.isTrigger = colliderSetting;
            }

            this.Enable();

            isAwake = true;
        }

        // Turn off interactable pieces
        private void Disable()
        {
            // If there is an area trigger, we want to disable it for now
            if (m_areaTrigger)
            {
                m_areaTrigger.gameObject.SetActive(false);
            }
        }

        // Turn on interactable pieces
        private void Enable()
        {
            // If there is an area trigger, we want to re-enable it
            if (m_areaTrigger)
            {
                m_areaTrigger.gameObject.SetActive(true);
            }
        }


        public virtual void Engage()
        {
            isEngaged = true;

            string text = null;
            State currentState = this.GetCurrentState();
            if (currentState)
            {
                text = currentState.eventText;
                Storyline.Log(text, currentState.explainText);
            }
            
            switch (type)
            {
            case ELEMENT_PROXIMITY.DISTANT:
                break;
            case ELEMENT_PROXIMITY.NEAR:
            default:
                this.DisplayText(text); // need a way to only show one display at a time ... with smart level design?
                break;
            }
        }

        public virtual void Disengage()
        {
            isEngaged = false;
            this.HideDisplay();
        }

        public virtual void Interact()
        {
            State currentState = this.GetCurrentState();

            if (stopAnimationAfterInteraction)
            {
                m_Animator.SetBool("m_Disable", true);
            }

            if (currentState)
            {
                if (currentState.Activate())
                {
                    AdvanceState();
                }
            }
            else
            {
                if (m_Debug) { Debug.Log(this.name + " could not find current state " + m_currentStateIdx); } 
            }
        }

        // Advance to next state or specified state, skipping all transitions
        public void AdvanceState(State nextState = null)
        {
            State currentState = this.GetCurrentState();

            if (currentState)
            {
                Invoke("DoExit", currentState.exitDelaySeconds);
            }

            // Move to next state
            if (!nextState)
            {
                // This should protect us from incrementing above current state
                m_currentStateIdx++;

                nextState = this.GetCurrentState();
            }
            else
            {
                throw new System.NotImplementedException();
            }

            if (nextState)
            {
                Storyline.Log(nextState.eventText, nextState.explainText);

                Invoke("DoTransition", nextState.entryDelaySeconds);
            }
        }

        // Take care of any state exit stuff
        protected virtual void DoExit()
        {
            // nothing yet
        }

        // Make changes defined by transition into entry of next state
        private void DoTransition()
        {
            // Set transition parameter defaults
            string newText = null;

            // Get current state parameters, if it exists
            State newState = this.GetCurrentState();
            if (newState)
            {
                if (m_Debug) { Debug.Log("Starting transition to " + newState.name); }

                newText = newState.eventText; /** TMP **/

                // Play animation
                if (newState.animationOnEntry)
                {
                    // Note: the specified animation must be added to the
                    // Animation component's list of Animations
                    m_Animation.Play(newState.animationOnEntry.name);
                }

                // Play sound
                if (newState.audioOnEntry)
                {
                    AudioManager.PlaySfx(newState.audioOnEntry);
                }

                // Change sprite
                if (newState.spriteOnEntry)
                {
                    // TMP - Disable animator, or we won't change sprite
                    m_Animator.enabled = false;
                    m_Sprite.sprite = newState.spriteOnEntry;
                }

                // Add instability
                //Unstable.OnUnstableAdded?.Invoke(newState.entropyValue);
                Unstable.AddEntropy(newState.entropyValue);

                Notify();
            }

            if (isEngaged)
            {
                // Make changes
                switch (type)
                {
                    case ELEMENT_PROXIMITY.NEAR:
                        this.DisplayText(newText); /** TMP **/
                        break;
                    default:
                        break;
                }
            }
        }

        private void Notify()
        {
            State currentState = this.GetCurrentState();
            if (currentState)
            {
                State.OnStateChange?.Invoke(currentState);
            }
            else
            {
                State.OnStateChange?.Invoke(null, this.displayText);
            }
        }

    }
}
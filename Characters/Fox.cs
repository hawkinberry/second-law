using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Consequences
{
    public class Fox : Character
    {
        protected override void Start()
        {
            base.Start();

            motionType = MOTION_TYPE.STATIC; // start motionless
        }

        public override void AreaEntered()
        {
            base.AreaEntered();

            Debug.Log("You scared the fox!");
            motionType = MOTION_TYPE.CONSTANT; // enable movement
            m_playNow = true;
        }

    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Consequences
{
    using Elements;
    using Utility.Audio;

    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(Collider2D))]
    public class Character : Element
    {
        public enum MOTION_TYPE
        {
            STATIC,
            CONSTANT,
            WANDER,
            PATH,
            CONTROLLER
        };

        [Header("Sprite")]
        public bool m_FacingRight = true;

        [Header("Sound")]
        public AudioClip playOnAwake;
        protected bool m_playNow;

        [Header("Movement")]
        public MOTION_TYPE motionType;

        [SerializeField] protected float m_MovementSpeed;
        [SerializeField] protected Vector2 m_Direction;
        // How much to smooth out diagonal movement
        [Range(0.2f, 1f)] [SerializeField] protected float m_MovementSmoothing = .7f;
        protected Vector2 currentVelocity;

        protected Rigidbody2D m_Body;
        protected Collider2D m_Collider;

        // Start is called before the first frame update
        protected override void Start()
        {
            base.Start();

            m_Body = GetComponent<Rigidbody2D>();
            m_Collider = GetComponent<Collider2D>();
            m_Animator = GetComponentInChildren<Animator>(true); // search in disabled components

            m_playNow = false;
        }

        protected virtual void FixedUpdate()
        {
            if (isAwake)
            {
                // HACK because state entry audio plays
                // even if element is in another scene
                if (playOnAwake && m_playNow)
                {
                    m_playNow = false;
                    AudioManager.PlaySfx(playOnAwake);
                }

                if (motionType == MOTION_TYPE.CONSTANT)
                {
                    this.Move(m_Direction);
                }
            }
        }

        protected virtual void Update()
        {
            if (isAwake)
            {
                if ((currentVelocity.x > 0 && !m_FacingRight) ||
                     (currentVelocity.x < 0 && m_FacingRight))
                {
                    Flip();
                }
            }
        }

        protected void CalculateVelocity(Vector2 direction)
        {
            currentVelocity = direction * m_MovementSpeed * Time.fixedDeltaTime;

            // slow down diagonal movement to make it consistent
            if (Mathf.Abs(currentVelocity.x) > 0 && Mathf.Abs(currentVelocity.y) > 0)
            {
                currentVelocity *= m_MovementSmoothing;
            }

            m_Animator.SetFloat("m_Speed", Mathf.Abs(currentVelocity.magnitude));
        }

        protected void DoMove(Vector2 velocity)
        {
            m_Body.MovePosition((Vector2)(transform.position) + velocity);
        }

        public virtual void Move(Vector2 direction)
        {
            CalculateVelocity(direction);
            DoMove(currentVelocity);
        }

        // Only flips the sprite, not the object
        protected virtual void Flip()
        {
            m_FacingRight = !m_FacingRight;

            Vector3 scale = m_Sprite.transform.localScale;
            scale.x *= -1;
            m_Sprite.transform.localScale = scale;
        }

        //public virtual void AreaEntered(Collider2D collision)
        public override void AreaEntered()
        {
            State currentState = this.GetCurrentState();

            if (currentState)
            {
                currentState.Activate(true);
            }
        }


        // Check whether player is facing given transform position
        public bool FacingObject(GameObject o)
        {
            // We aren't actually flipping the player object when we turn (because that disrupts hover text)
            // So we have to use m_FacingRight
            Vector3 forward = o.transform.right;
            if (!m_FacingRight)
            {
                forward *= -1;
            }

            float dot = Vector3.Dot(forward, (o.transform.position - transform.position).normalized);

            return (dot > 0); // positive dot product if we are pointing in the same direction
        }

        // 0 for x, 1 for y
        public float GetHitboxSize(int dim = 0)
        {
            if (dim == 0)
            {
                return m_Collider.bounds.size.x;
            }
            else if (dim == 1)
            {
                return m_Collider.bounds.size.y;
            }

            return 0;
        }

    }
}

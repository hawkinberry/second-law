using UnityEngine;
using UnityEngine.EventSystems;
using Utility.Actor.Router;

namespace Consequences
{
    // Based on https://medium.com/nerd-for-tech/point-click-to-move-in-unity-2021-98a8d682737f
    public class MouseController : Utility.Actor.Controller
    {
        [SerializeField] private Player m_Player;

        public AudioClip AlertUnable;

        private Camera m_Camera;

        private Vector2 NO_INPUT = Vector2.negativeInfinity;
        private bool m_hasInput;
        private bool m_InteractOnArrival;

        public Router2d m_Router;

        private int clickableMask;

        void Start()
        {
            m_Camera = Camera.main;
            clickableMask = 1 << LayerMask.NameToLayer("Clickable");
        }

        public override void Reset()
        {
            m_Router.Stop();

            // Push the character forward a little to preserve illusion of walking into transition
            Vector2 push = new Vector2(0.5f, 0);
            if (!m_Player.m_FacingRight)
            {
                push *= -1;
            }

            m_Router.Push(push);
        }

        // Update is called once per frame
        void Update()
        {
            HandleInput();

            DoMovement();
        }

        private void HandleInput()
        {
            // Primary Click
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit2D hit;
                
                Vector2 position = ScreenToWorldPoint(Input.mousePosition);

                // If we are over a UI element
                // This is an oddly named function, but it seems to do the trick
                if (EventSystem.current.IsPointerOverGameObject())
                {
                    return;
                }

                if (hit = Physics2D.Raycast(position, Vector3.back, clickableMask))
                {
                    Vector2 target = hit.point;

                    // we clicked an object
                    if (CustomCursor.CursorOverElement(out hit))
                    {
                        Debug.Log("Clicked an object");

                        // Get edge of object closer to player on straight line to click
                        target = GetEdgeOfElement(hit.point);
                        m_Router.SetNewWaypoint(target);
                    }
                    else
                    {
                        m_Router.SetNewWaypoint(target, 0.05f);
                    }
                    
                    Debug.Log("Got hit at " + target + "!");
                }
                else // Not an area that should be routed to
                {
                    //Debug.Log("No.");
                    Utility.Audio.AudioManager.PlaySfx(AlertUnable);
                }
            }
            // Right Click
            else if (Input.GetMouseButtonDown(1))
            {
                m_Player.InteractWithSelectedObject();
            }

        }

        private Vector2 GetEdgeOfElement(Vector2 clickPoint)
        {
            Vector2 currentPosition = (Vector2)transform.position;
            Vector2 target;
            RaycastHit2D hit;

            // Layers to ignore
            int intersectionMask = ~(Player.layerMask | 1 << LayerMask.NameToLayer("UI") | clickableMask);
            hit = Physics2D.Raycast(currentPosition, clickPoint - currentPosition, float.PositiveInfinity, intersectionMask);

            target = hit.point;
            
            // offset by size of hitbox
            //target.x += m_Player.GetHitboxSize(0) / 2 * -1 * Mathf.Sign((hit.point - currentPosition).x);
            //target.y += m_Player.GetHitboxSize(1) / 2 * -1 * Mathf.Sign((hit.point - currentPosition).y);

            return target;
        }

        private void DoMovement()
        {
            Vector2 movement = m_Router.GetDirection();

            m_Player.Move(movement);
        }

        public Vector2 ScreenToWorldPoint(Vector2 screenPosition)
        {
            Vector2 worldPosition = m_Camera.ScreenToWorldPoint(screenPosition);

            return worldPosition;
        }
    }
}
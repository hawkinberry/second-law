using UnityEngine;

namespace Consequences
{
    public class KeyboardController : Utility.Actor.Controller
    {
        [SerializeField] private Player m_Player;

        public override void Reset()
        {
            // do nothing
        }

        // Update is called once per frame
        void Update()
        {
            Vector2 movement = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

            m_Player.Move(movement);

            if (Input.GetButtonDown("Interact"))
            {
                m_Player.InteractWithSelectedObject();
            }
        }
    }
}
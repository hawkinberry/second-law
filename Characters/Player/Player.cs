using UnityEngine;
//using TMPro;
using System.Collections.Generic;

namespace Consequences
{
    using Elements;
    using Utility.Audio;
    using Utility.Actor;

    public class Player : Character
    {
        public static int layerMask;

        private bool m_isEnabled;

        public Controller m_Controller;

        // TUTORIAL
        private bool m_FirstMove = false;
        private bool m_FirstInteract = true;
        public string HELP_TEXT = "Press SPACE to interact.";

        private bool m_SoundPlaying = false;
        private float m_StepInterval;

        private AudioSource m_Audio;

        private List<Element> availableElements;
        private Element currentElement;

        #region States
        private bool m_isEngaged;
        public bool isEngaged
        {
            get { return m_isEngaged; }
            set
            {
                m_isEngaged = value;
                m_Animator.SetBool("m_isEngaged", value);
            }
        }

        public bool isMoving
        {
            get { return currentVelocity.magnitude > 0.01f; }
        }

        public bool isCrouched
        {
            get
            {
                // Maybe a way to do this with layers?
                return (m_Animator.GetCurrentAnimatorStateInfo(0).IsName("CharacterCrouch")
                    || m_Animator.GetCurrentAnimatorStateInfo(0).IsName("CharacterStand")
                    || m_Animator.GetCurrentAnimatorStateInfo(0).IsName("CharacterCrouchIdle"));
            }
        }

        // Determine whether a collider is owned by the player
        public static bool IsPlayer(Collider2D c)
        {
            return c.gameObject.name.Contains("Player");
        }
        #endregion

        private void OnEnable()
        {
            AreaTrigger.OnAreaEnter += ApproachObject;
            AreaTrigger.OnAreaExit += LeaveObject;
            State.OnStateChange += RespondToState;

            // Events to restrict player movement
            Utility.Executive.OnGameResume += Enable;
            HistoryScreen.OnHistoryClose += Enable;
            HistoryScreen.OnHistoryOpen += Disable;
            Utility.Executive.OnGamePause += Disable;
            Utility.Executive.OnGameOver += Disable;
        }

        private void OnDisable()
        {
            AreaTrigger.OnAreaEnter -= ApproachObject;
            AreaTrigger.OnAreaExit -= LeaveObject;
            State.OnStateChange -= RespondToState;

            Utility.Executive.OnGameResume -= Enable;
            HistoryScreen.OnHistoryClose -= Enable;
            HistoryScreen.OnHistoryOpen -= Disable;
            Utility.Executive.OnGamePause -= Disable;
            Utility.Executive.OnGameOver -= Disable;
        }

        // Start is called before the first frame update
        protected override void Start()
        {
            layerMask = 1 << LayerMask.NameToLayer("Player");

            base.Start();

            m_Audio = GetComponent<AudioSource>();
            availableElements = new List<Element>();

            AnimationClip[] clips = m_Animator.runtimeAnimatorController.animationClips;
            foreach (AnimationClip a in clips)
            {
                if (a.name.Contains("Move"))
                {
                    m_StepInterval = a.length / 2f;
                }
            }

            AudioManager.RegisterSFXSource(ref m_Audio);

            m_isEnabled = true;
        }

        private void Enable()
        {
            m_isEnabled = true;
        }

        private void Disable()
        {
            m_isEnabled = false;
        }

        // Move the player at a position. Null any prior routes
        public void Spawn(Vector2 position)
        {
            transform.position = position;
            m_Controller.Reset();
        }

        private void Walking(bool enable)
        {
            if (enable)
            {
                if (!m_SoundPlaying)
                {
                    m_SoundPlaying = true;
                    InvokeRepeating("PlayStep", 0f, m_StepInterval);
                }
            }
            else
            {
                m_SoundPlaying = false;
                CancelInvoke("PlayStep");
            }
        }

        private void PlayStep()
        {
            if (Environments.EnvironmentManager.GetCurrentEnvironment())
            {
                AudioClip step = Environments.EnvironmentManager.GetCurrentEnvironment().m_footStep;
                if (step)
                {
                    m_Audio.PlayOneShot(step, AudioManager.SFX_VOLUME * 0.5f);
                }
            }
        }

        #region Character Methods
        public override void Move(Vector2 direction)
        {
            if (m_isEnabled)
            {
                CalculateVelocity(direction);

                Walking(isMoving && !isCrouched);

                // filtering allows us to support multiple controllers at once
                if (direction == Vector2.zero)
                {
                    return;
                }

                if (!m_FirstMove && direction != Vector2.zero)
                {
                    m_FirstMove = true;
                    Utility.Executive.OnGameStart?.Invoke();
                }

                if (!isCrouched)
                {
                    DoMove(currentVelocity);
                    CheckFacing();
                }
                else
                {
                    DoCrouch();
                }

            }
        }

        // Only flips the sprite, not the object
        protected override void Flip()
        {
            base.Flip();

            // Check if we are still engaged and should break
            CheckFacing();
        }
        #endregion

        private void DoCrouch()
        {
            if (!currentElement)
            {
                SelectElement();
            }
        }

        private void CheckFacing()
        {
            if (currentElement)
            {
                if (!FacingObject(currentElement.gameObject))
                {
                    RemoveFocus(currentElement);
                }
            }
            else
            {
                SelectElement(); // find an element, if available
            }
        }

        private void SelectElement()
        {
            // Loop through available objects
            // Engage with closest facing
            Element select = null;
            foreach (Element e in availableElements)
            {
                if (FacingObject(e.gameObject))
                {
                    select = e;
                }
            }

            if (select)
            {
                Focus(select);
            }
        }

        private void Focus(Element e)
        {
            if (m_Debug) { Debug.Log(this.name + " focusing on " + e.name); }
            e.Engage();

            currentElement = e;
            isEngaged = true;

            // TUTORIAL for first interaction
            if (m_FirstInteract)
            {
                this.DisplayText(HELP_TEXT);
            }
            else if (e.type == Element.ELEMENT_PROXIMITY.DISTANT)
            {
                this.DisplayText(e.displayText);
            }
        }

        private void RemoveFocus(Element e)
        {
            if (m_Debug) { Debug.Log(this.name + " removing focus from " + e.name); }
            e.Disengage();
            this.HideDisplay();

            currentElement = null;
            isEngaged = false;
        }

        public void InteractWithSelectedObject()
        {
            if (currentElement)
            {
                if (m_FirstInteract)
                {
                    m_FirstInteract = false;
                    this.HideDisplay();
                }

                m_Animator.SetTrigger("m_Interacts");

                if (m_Debug) { Debug.Log("Interacting with " + currentElement.name); }
                currentElement.Interact();
            }
            else
            {
                if (m_Debug) { Debug.Log("No object selected."); }
            }
        }


        private void ApproachObject(IHandleArea obj)
        {
            Element element;

            if (element = (Element)obj)
            {
                availableElements.Add(element);

                // Only engage if we are facing the element
                if (FacingObject(element.gameObject))
                {
                    // TUTORIAL for first interaction
                    if (m_FirstInteract)
                    {
                        this.DisplayText(HELP_TEXT);
                    }
                    else if (element.type == Element.ELEMENT_PROXIMITY.DISTANT)
                    {
                        this.DisplayText(element.displayText);
                    }

                    // Engage but do not interact
                    isEngaged = true;
                }
            }
        }

        private void LeaveObject(IHandleArea obj)
        {
            Element element;

            if (element = (Element)obj)
            {
                availableElements.Remove(element);

                RemoveFocus(element);
            }
        }

        private void RespondToState(State newState, string altText = null)
        {
            if (currentElement)
            {
                if (currentElement.type == Element.ELEMENT_PROXIMITY.DISTANT)
                {
                    if (newState)
                    {
                        this.DisplayText(newState.eventText);
                    }
                    else if (altText != null)
                    {
                        this.DisplayText(altText);
                    }
                }
            }
        }
    }
}

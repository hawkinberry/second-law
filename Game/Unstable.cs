using UnityEngine;
using TMPro;

namespace Consequences
{
    using Utility.Menus;

    public class Unstable : MonoBehaviour
    {
        public static UnstableAddHandler OnUnstableAdded;
        public static RegistrationHandler OnRegister;

        // TEXT FILL
        public TextMeshProUGUI m_outcomeText;
        // Win condition
        public const string WIN_TEXT = "But you did";

        // Lose condition
        public const string LOSE_TEXT = "But you can try to";

        private static float m_entropyLimit;
        public static float DEFAULT_ENTROPY = 0f;
        
        public float minimumPossibleEntropy;
        public float maximumPossibleEntropy;
        public bool m_OverrideAutoSet = false;

        public Utility.Menus.FillBar entropyBar;
        public float fillSpeed = 1.0f;
        private bool m_drawBar;

        private static float m_globalEntropy;
        public static float globalEntropy
        {
            get { return m_globalEntropy; }
            set
            {
                m_globalEntropy = Mathf.Clamp(value, 0, m_entropyLimit);
            }
        }

        private void Start()
        {
            globalEntropy = 0; // reset static field
            m_drawBar = false;
            Debug.Log("Total available entropy: " + m_entropyLimit);

            // TMP
            if (m_OverrideAutoSet)
            {
                m_entropyLimit = maximumPossibleEntropy;
            }

            entropyBar.SetVal(0);
            entropyBar.SetMax(m_entropyLimit);
        }

        private void Update()
        {
            // Draw bar when enabled
            if (m_drawBar)
            {
                float increment = maximumPossibleEntropy * fillSpeed * Time.deltaTime;

                if ((entropyBar.Value + increment) <= globalEntropy)
                {
                    entropyBar.Add(increment);
                }
            }
        }

        public static void Register(float aVal)
        {
            m_entropyLimit += aVal;
        }

        private void OnEnable()
        {
            Utility.Executive.OnEndGame += StartDraw;
        }

        private void OnDisable()
        {
            Utility.Executive.OnEndGame -= StartDraw;
        }

        private void StartDraw()
        {
            m_drawBar = true;
            Debug.Log("Drawing a total of " + globalEntropy + " entropy.");

            if (globalEntropy == minimumPossibleEntropy)
            {
                m_outcomeText.text = WIN_TEXT;
            }
            else
            {
                m_outcomeText.text = LOSE_TEXT;
            }
        }

        private void CalculateTotal(float amount)
        {
            m_entropyLimit += amount;
        }

        public static void AddEntropy(float amount)
        {
            Debug.Log("Adding entropy " + amount);

            globalEntropy += amount;

            /* Disable check for now
            if (globalEntropy >= m_entropyLimit)
            {
                Menus.GameOverlay.OnLoseGame?.Invoke();
            }
            */
        }

        public delegate void UnstableAddHandler(float amount);
        public delegate void RegistrationHandler(float amount);
    }
}
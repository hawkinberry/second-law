using UnityEngine;
using UnityEngine.EventSystems;

namespace Consequences
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class CustomCursor : Utility.CustomCursor
    {
        private SpriteRenderer m_renderer;
        public Sprite m_default;
        public Sprite m_interact;

        private void Awake()
        {
            m_renderer = GetComponent<SpriteRenderer>();
        }

        protected override void Start()
        {
            base.Start();

            m_renderer.sprite = m_default;
        }

        protected override void Update()
        {
            base.Update();

            CheckPosition();
        }

        private void CheckPosition()
        {
            if (CursorOverElement())
            {
                m_renderer.sprite = m_interact;
            }
            else
            {
                m_renderer.sprite = m_default;
            }
        }

        public static bool CursorOverElement()
        {
            RaycastHit2D trash;
            return CursorOverElement(out trash);
        }

        // Blocks by UI 
        public static bool CursorOverElement(out RaycastHit2D hit)
        {
            Vector2 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            bool foundElement = false;
            hit = new RaycastHit2D{ }; // assign empty hit

            // If cursor is over UI .. function name doesn't make sense, but it does what we need
            if (EventSystem.current.IsPointerOverGameObject())
            {
                return foundElement;
            }
            
            RaycastHit2D[] hits;
            hits = Physics2D.RaycastAll(position, Vector3.forward);
            foreach (RaycastHit2D h in hits)
            {
                if (IsElement(h.collider))// we clicked an object
                {
                    //Debug.Log("Cursor found object " + hit.collider.gameObject.name);
                    foundElement = true;
                    hit = h;
                }
            }

            return foundElement;
        }

        public static bool IsElement(Collider2D collider)
        {
            return (collider.gameObject.layer == LayerMask.NameToLayer("Clickable")) && (collider.gameObject.name != "ClickableArea");
            //return (collider.gameObject.name != "ClickableArea");
            //return (collider.gameObject.GetType() == typeof(Elements.AreaTrigger));
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utility.Audio
{
    //[RequireComponent(typeof(AudioSource))]
    // Based on https://www.youtube.com/watch?v=8pFlnyfRfRc
    public class AudioManager : MonoBehaviour
    {
        static bool m_MusicMuted = false;
        static bool m_MusicPaused = false;
        public static bool IsMusicMuted { get { return m_MusicMuted; } }

        static bool m_SfxMuted = false;
        static bool m_SfxPaused = false;
        public static bool IsSfxMuted { get { return m_SfxMuted; } }

        static public AudioManager m_Instance;

        // Audio clip collections
        static List<AudioClip> m_sounds = new List<AudioClip>();

        // Single shots
        static AudioClip m_Button;
        static AudioClip m_footstep;

        // Music
        public static int mLastMusicPosition;

        // Audio sources
        private static AudioSource m_SfxSource;
        private static AudioSource m_MusicPlayer;

        // Audio source collections
        private static List<AudioSource> m_Sources;
        private static List<AudioSource> m_SfxSources;
        private static List<AudioSource> m_MusicSources;

        // Configurable volume settings
        public static float VOLUME = 1.0f;
        public static float SFX_VOLUME = 1.0f;
        public static float MUSIC_VOLUME = 1.0f;

        // Volume profiles
        private const float MUSIC_BALANCE = 0.5f;

        public static void RegisterMusicSource(ref AudioSource aSource)
        {
            m_MusicSources.Add(aSource);
            m_Sources.Add(aSource);
        }

        public static void RegisterSFXSource(ref AudioSource aSource)
        {
            m_SfxSources.Add(aSource);
            m_Sources.Add(aSource);
        }

        private static void SetVolume(ref List<AudioSource> rSources, float aSetting)
        {
            foreach (AudioSource src in rSources)
            {
                src.volume = aSetting;
            }
        }

        public static void SetAllVolume(float input)
        {
            VOLUME = input;
            SetVolume(ref m_Sources, VOLUME);
        }

        public static void SetSfxVolume(float input)
        {
            SFX_VOLUME = input;
            SetVolume(ref m_SfxSources, Mathf.Min(VOLUME, SFX_VOLUME));
        }

        public static void SetMusicVolume(float input)
        {
            MUSIC_VOLUME = input;
            SetVolume(ref m_MusicSources, Mathf.Min(VOLUME, MUSIC_VOLUME));
        }

        void Awake()
        {
            m_Instance = this;

            m_SfxSource = gameObject.transform.Find("SFX").GetComponent<AudioSource>();
            m_MusicPlayer = gameObject.transform.Find("Music").GetComponent<AudioSource>();

            // I would like a more modular way to do this
            m_Sources = new List<AudioSource> { m_SfxSource, m_MusicPlayer };
            m_SfxSources = new List<AudioSource> { m_SfxSource };
            m_MusicSources = new List<AudioSource> { m_MusicPlayer };
        }

        // Start is called before the first frame update
        void Start()
        {
            // Load samples
            m_Button = Resources.Load<AudioClip>("Sounds/buttonSelect");

            SetSfxVolume(SFX_VOLUME);
            SetMusicVolume(0); // start off and fade in

            m_MusicPlayer.enabled = true;
            m_MusicPlayer.Play();
            StartCoroutine(StartFade(m_MusicPlayer, 10f, MUSIC_BALANCE));
        }

        // See https://gamedevbeginner.com/how-to-fade-audio-in-unity-i-tested-every-method-this-ones-the-best/
        public static IEnumerator StartFade(AudioSource audioSource, float duration, float targetVolume)
        {
            float currentTime = 0;
            float start = audioSource.volume;

            while (currentTime < duration)
            {
                currentTime += Time.deltaTime;
                audioSource.volume = Mathf.Lerp(start, targetVolume, currentTime / duration);
                yield return null;
            }
            yield break;
        }

        public static void SetFootstep(AudioClip sound)
        {
            m_footstep = sound;
        }

        public static bool ToggleMusicMute()
        {
            SetMusicMute(!m_MusicMuted);
            return m_MusicMuted;
        }

        public static void SetMusicMute(bool setting)
        {
            m_MusicMuted = setting;

            foreach (AudioSource s in m_MusicSources)
            {
                s.mute = m_MusicMuted;
            }
        }

        public static bool ToggleSfxMute()
        {
            SetSfxMute(!m_SfxMuted);
            return m_SfxMuted;
        }

        public static void SetSfxMute(bool setting)
        {
            m_SfxMuted = setting;

            foreach (AudioSource s in m_SfxSources)
            {
                s.mute = m_SfxMuted;
            }
        }

        public static void SetPause(bool pause)
        {
            m_SfxPaused = pause;
            m_MusicPaused = pause;
            foreach (AudioSource s in m_Sources)
            {
                if (pause)
                {
                    s.Pause();
                }
                else
                {
                    s.UnPause();
                }
            }
        }

        public static void PlaySfx(AudioClip clip, bool loop = false)
        {
            if (m_SfxPaused)
            {
                // Play calls will force current source to un-pause
                return;
            }

            if (clip)
            {
                if (loop)
                {
                    m_SfxSource.clip = clip;
                    m_SfxSource.Play();
                }
                else
                {
                    m_SfxSource.PlayOneShot(clip, SFX_VOLUME);
                }
            }
            else
            {
                Debug.Log("Cannot play null sfx clip.");
            }
        }

        public static void StopSfx()
        {
            m_SfxSource.Stop();
        }

        public static void PlayMusic(AudioClip clip, bool loop = false, bool resume = false)
        {
            if (m_MusicPaused)
            {
                // Play calls will force current source to un-pause
                return;
            }

            if (clip)
            {
                if (loop)
                {
                    m_MusicPlayer.clip = clip;
                    if (resume)
                    {
                        m_MusicPlayer.timeSamples = mLastMusicPosition;
                    }
                    m_MusicPlayer.Play();
                }
                else
                {
                    m_MusicPlayer.PlayOneShot(clip, MUSIC_VOLUME);
                }
            }
            else
            {
                Debug.Log("Cannot play null sfx clip.");
            }
        }

        public static void StopMusic(bool resume = false)
        {
            mLastMusicPosition = m_MusicPlayer.timeSamples;
            m_MusicPlayer.Stop();
        }

        public static void PlaySound(string clip)
        {
            switch (clip)
            {
                case "button":
                    PlayButtonPress();
                    break;
                case "footstep":
                    PlayFootstep();
                    break;
                default:
                    Debug.Log("Invalid audio clip " + clip + " requested");
                    break;
            }
        }

        private static AudioClip PlayRandomSound(ref AudioSource aSource, ref List<AudioClip> aList, float aVolume)
        {
            int choice = Random.Range(0, aList.Count);
            aSource.PlayOneShot(aList[choice], aVolume);

            return aList[choice];
        }

        private static void PlayButtonPress()
        {
            m_SfxSource.PlayOneShot(m_Button, SFX_VOLUME * 0.5f);
        }

        private static void PlayFootstep()
        {
            m_SfxSource.PlayOneShot(m_footstep, SFX_VOLUME * 0.2f);
        }
    }
}

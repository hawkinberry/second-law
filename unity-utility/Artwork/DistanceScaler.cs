using UnityEngine;


namespace Utility.Artwork
{
    public class DistanceScaler : MonoBehaviour
    {
        public float maxY = 0f;
        public float minY = -6f;

        public float maxScale = 1f;
        public float minScale = 0.5f;

        private float yRange;
        private float scaleRange;

        private void Awake()
        {
            // Doing this once at the start means the limits
            // should not change over the course of the game!
            yRange = maxY - minY;
            scaleRange = maxScale - minScale;
        }

        // Update is called once per frame
        void Update()
        {
            float currentY = transform.parent.transform.position.y;
            // Calculate the extent along the vertical range of movement, using the "minY" as "closest to the camera"
            // percentRange = 1 when currentY = minY
            // percentRange = 0 when currentY = maxY
            float percentRange = Mathf.Abs(maxY - currentY) / yRange;
            
            // Use the vertical extent to calculate scale along range of scaling
            // scale = maxScale when percentRange = 1
            // scale = minScale when percentRange = 0
            float newScale = (percentRange * scaleRange) + minScale;

            // Set new scale, preserving sign of x and y, and preserving z
            transform.localScale = new Vector3(newScale * Mathf.Sign(transform.localScale.x), 
                                               newScale * Mathf.Sign(transform.localScale.y), 
                                               transform.localScale.z);
        }
    }
}
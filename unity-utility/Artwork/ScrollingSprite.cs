using UnityEngine;

namespace Utility.Artwork
{
    // Sprite Renderer must be set in Draw Mode 'Tiled' and Tile Mode 'Continuous'
    [RequireComponent(typeof(SpriteRenderer))]
    public class ScrollingSprite : MonoBehaviour
    {
        [SerializeField] private float speed = 0.5f;

        private SpriteRenderer m_Sprite;
        private float spriteSize;
        private float startX;
        // Start is called before the first frame update
        void Start()
        {
            m_Sprite = GetComponent<SpriteRenderer>();
            spriteSize = m_Sprite.size.x;
            startX = m_Sprite.transform.position.x;
        }

        // Update is called once per frame
        void Update()
        {
            Vector3 movement = transform.right * Time.deltaTime * speed;
            float currentX = m_Sprite.transform.position.x;

            // We scale the sprite horizontally to achieve scrolling
            // But we need to counteract the scaling, effectively locking the "leading" edge of the gameObject
            SetPosition(currentX - movement.x);
            SetWidth(m_Sprite.size.x + movement.x * 2);

            // Check if this has moved the sprite past its boundary
            
            if (Mathf.Abs(currentX) > spriteSize)
            {
                // Reset position
                SetPosition(startX);
                SetWidth(spriteSize);
            }
        }

        private void SetPosition(float x)
        {
            m_Sprite.transform.position = new Vector3(x, m_Sprite.transform.position.y, m_Sprite.transform.position.z);
        }

        private void SetWidth(float width)
        {
            m_Sprite.size = new Vector2(width, m_Sprite.size.y);
        }
    }
}
using UnityEngine;

namespace Utility
{
    public class CustomCursor : MonoBehaviour
    {
        protected virtual void Start()
        {
            // Hide normal cursor
            Cursor.visible = false;
        }

        protected virtual void Update()
        {
            transform.position = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }
    }
}

﻿using UnityEngine;

namespace Utility
{
    public static class Executive
    {
        public enum GAME_STATE
        {
            INIT = 0,
            PLAY,
            PAUSE,
            OVER
        };

        // Game Events
        public static GameExecutiveHandler OnGameStart;
        public static GameExecutiveHandler OnGamePause;
        public static GameExecutiveHandler OnGameResume;
        public static GameExecutiveHandler OnEndGame;
        public static GameExecutiveHandler OnLoseGame;
        public static GameExecutiveHandler OnGameOver;

        public static float TimeScale = Time.timeScale;
        public static GAME_STATE GameState;

        public static void SetPause(bool paused)
        {
            Time.timeScale = (paused ? 0f : 1f);
            GameState = (paused ? GAME_STATE.PAUSE : GAME_STATE.PLAY);
        }

        public static bool IsPaused()
        {
            return GameState == GAME_STATE.PAUSE;
        }

        // Game Delegates
        public delegate void GameExecutiveHandler();
    }
}

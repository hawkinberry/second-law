﻿using System.Collections;
using UnityEngine;

namespace Utility
{
    public class CameraShake : MonoBehaviour
    {
        [SerializeField] private float m_DefaultDuration = .15f;
        [SerializeField] private float m_DefaultMagnitude = .4f;

        public IEnumerator Shake()
        {
            return Shake(m_DefaultDuration, m_DefaultMagnitude);
        }

        public IEnumerator Shake(float aDuration, float aMagnitude)
        {
            Vector3 originalPosition = transform.localPosition;

            float timeElapsed = 0f;
            while (timeElapsed < aDuration)
            {
                float xOffset = Random.Range(-1f, 1f) * aMagnitude;
                float yOffset = Random.Range(-1f, 1f) * aMagnitude;

                transform.localPosition = new Vector3(xOffset, yOffset, originalPosition.z);
                timeElapsed += Time.deltaTime;

                // Run alongside Update()
                yield return new WaitForEndOfFrame();
            }

            transform.localPosition = originalPosition;
        }
    }
}
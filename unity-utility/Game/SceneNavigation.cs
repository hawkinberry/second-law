﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Utility
{
    public class SceneNavigation : MonoBehaviour
    {
        public string m_MainMenuScene = "MainMenu";
        public string m_CreditsScene = "Credits";

        public void OnMainMenuButtonPressed()
        {
            // In case we are coming here from a pause menu
            Time.timeScale = 1f;
            SceneManager.LoadScene(m_MainMenuScene);
        }

        public void OnCreditsButtonPressed()
        {
            SceneManager.LoadScene("Credits");
            OnButtonPress();
        }

        public static void OnButtonPress()
        {
            Audio.AudioManager.PlaySound("button");
        }

        public void OnQuitButtonPressed()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
            Application.Quit();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utility.Actor.Router
{
	public struct Boundary3d
	{
		public float max_X;
		public float min_X;
		public float max_Y;
		public float min_Y;
		public float max_Z;
		public float min_Z;
	}

	public abstract class Router3d : MonoBehaviour
	{

		public static Vector3 NO_INPUT = Vector3.negativeInfinity;

		[SerializeField] float searchBuffer = 0.1f;
		public Boundary3d keepInZone;

		//[Range(0, 1f)] [SerializeField] private float m_ChanceToIdle = .5f;
		//private float m_IdleTimeout = 0.0f;

		// See https://answers.unity.com/questions/57067/make-objects-walk-around-randomly.html
		protected Vector3 targetWaypoint;
		protected Vector3 newWaypoint;

		protected float time;
		[SerializeField] protected float m_GoalSensitivity = 0.1f;

		public virtual Vector3 SetNewWaypoint(Vector3 waypoint)
		{
			float min_X, max_X, min_Y, max_Y, min_Z, max_Z;
			min_X = keepInZone.min_X + searchBuffer;
			max_X = keepInZone.max_X - searchBuffer;
			min_Y = keepInZone.min_Y + searchBuffer;
			max_Y = keepInZone.max_Y - searchBuffer;
			min_Z = keepInZone.min_Z + searchBuffer;
			max_Z = keepInZone.max_Z - searchBuffer;

			Vector3 newWaypoint = new Vector3(Random.Range(min_X, max_X),
											  Random.Range(min_Y, max_Y),
											  Random.Range(min_Z, max_Z));


			targetWaypoint = waypoint;
			//Debug.Log("New waypoint: " + targetWaypoint);

			return targetWaypoint;
		}

		public abstract Vector3 GetDirection();

	}
}
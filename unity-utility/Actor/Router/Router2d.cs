﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utility.Actor.Router
{
	public struct Boundary2d
	{
		public float max_X;
		public float min_X;
		public float max_Y;
		public float min_Y;
	}

	public abstract class Router2d : MonoBehaviour
	{

		public static Vector2 NO_INPUT = new Vector2(float.NaN, float.NaN);

		[SerializeField] float searchBuffer = 0.1f;
		public Boundary2d keepInZone;

		// See https://answers.unity.com/questions/57067/make-objects-walk-around-randomly.html
		protected Vector2 targetWaypoint;
		protected Vector2 newWaypoint;

		protected float time;
		[SerializeField] protected float m_DefaultGoalSensitivity = 0.1f;
		protected float m_GoalSensitivity;
		private const float cUSE_DEFAULT = -1;

        private void Start()
        {
			targetWaypoint = transform.position;
        }

        public bool HasInput()
		{
			return (targetWaypoint.x != float.NaN && targetWaypoint.y != float.NaN);

		}

		public virtual Vector2 SetNewWaypoint(Vector2 waypoint, float sensitivity = cUSE_DEFAULT)
		{
			float min_X, max_X, min_Y, max_Y;
			min_X = keepInZone.min_X + searchBuffer;
			max_X = keepInZone.max_X - searchBuffer;
			min_Y = keepInZone.min_Y + searchBuffer;
			max_Y = keepInZone.max_Y - searchBuffer;

			if (sensitivity == cUSE_DEFAULT)
            {
				m_GoalSensitivity = m_DefaultGoalSensitivity;
            }
			else
            {
				m_GoalSensitivity = sensitivity;
            }

			targetWaypoint = waypoint;

			return targetWaypoint;
		}

		public void Push(Vector2 push)
        {
			SetNewWaypoint((Vector2)transform.position + push);
        }

		public abstract Vector2 GetDirection();
		public abstract void Stop();
	}
}
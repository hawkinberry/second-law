﻿using UnityEngine;

namespace Utility.Actor.Router
{
	public class AutoRouter2d : Router2d
	{
		public override Vector2 GetDirection()
		{
			Vector2 return_input = Vector2.zero;
			Vector2 currentPosition = transform.position;

			// If the bot reaches the point, find a new waypoint
			float distanceToGo = Vector2.Distance(currentPosition, targetWaypoint);
			
			if (distanceToGo > m_GoalSensitivity)
			{
				//Debug.Log("Waypoint distance: " + distanceToGo);
				//Debug.Log("Routing: " + transform.position + " => " + targetWaypoint);
				return_input = (targetWaypoint - currentPosition).normalized;
			}

			//Debug.Log("Input = " + return_input);

			return return_input;
		}

        public override void Stop()
        {
			targetWaypoint = transform.position;
        }
    }
}
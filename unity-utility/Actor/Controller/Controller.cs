using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utility.Actor
{
    public abstract class Controller : MonoBehaviour
    {
        public abstract void Reset();
    }
}
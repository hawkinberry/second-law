﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Utility.Menus
{
    public class GameOverlay : MonoBehaviour
    {
        [Header("Sound Controls")]
        [SerializeField] private GameObject m_MusicOnButton;
        [SerializeField] private GameObject m_MusicOffButton;
        [SerializeField] private GameObject m_SfxOnButton;
        [SerializeField] private GameObject m_SfxOffButton;

        [Header("Menus")]
        [SerializeField] private StartMenu m_StartMenu;
        [SerializeField] private PauseMenu m_PauseMenu;
        [SerializeField] private SettingsMenu m_SettingsMenu;
        [SerializeField] private EndScreen m_EndScreen;

        private bool m_Enable = true;

        #region Initialize
        private void OnEnable()
        {
            // Register delegates
            Executive.OnLoseGame += EndGame;
            Executive.OnGameOver += EndGame;
        }

        private void OnDisable()
        {
            // Deregister delegates
            Executive.OnLoseGame -= EndGame;
            Executive.OnGameOver -= EndGame;
        }

        void Start()
        {
            MusicOn();
            SfxOn();
        }
        #endregion

        #region Settings
        public void OnSettings()
        {
            m_PauseMenu.Hide();
            m_SettingsMenu.Show();
        }

        public void OnSettingsBack()
        {
            m_PauseMenu.Show();
            m_SettingsMenu.Hide();
        }
        #endregion


        public void StartGame()
        {
            m_StartMenu.StartTransition();
        }

        private void EndGame()
        {
            Executive.OnEndGame?.Invoke();
        }

        public void Disable()
        {
            m_Enable = false;
        }

        public void MusicOn()
        {
            Audio.AudioManager.SetMusicMute(false);

            m_MusicOffButton.SetActive(true);
            m_MusicOnButton.SetActive(false);
        }

        public void MusicOff()
        {
            Audio.AudioManager.SetMusicMute(true);

            m_MusicOffButton.SetActive(false);
            m_MusicOnButton.SetActive(true);
        }

        public void SfxOn()
        {
            Audio.AudioManager.SetSfxMute(false);

            m_SfxOffButton.SetActive(true);
            m_SfxOnButton.SetActive(false);
        }

        public void SfxOff()
        {
            Audio.AudioManager.SetSfxMute(true);

            m_SfxOffButton.SetActive(false);
            m_SfxOnButton.SetActive(true);
        }

        public void OnMusicButtonPressed()
        {
            bool music_muted = Audio.AudioManager.ToggleMusicMute();

            m_MusicOffButton.SetActive(!music_muted);
            m_MusicOnButton.SetActive(music_muted);
        }

        public void OnSfxButtonPressed()
        {
            bool sfx_muted = Audio.AudioManager.ToggleSfxMute();

            m_SfxOffButton.SetActive(!sfx_muted);
            m_SfxOnButton.SetActive(sfx_muted);
        }
    }
}

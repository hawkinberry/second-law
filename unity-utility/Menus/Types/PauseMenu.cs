using UnityEngine;

namespace Utility.Menus
{
    public class PauseMenu : BaseMenu
    {
        private bool m_GamePaused = false;
        private bool m_GameStarted = false; // Only allow pausing when game has started

        void OnEnable()
        {
            Executive.OnGameStart += Activate;
            //Executive.OnGameOver += Disable;
        }

        void OnDisable()
        {
            Executive.OnGameStart -= Activate;
            //Executive.OnGameOver -= Disable;
        }

        public void Activate()
        {
            m_GameStarted = true;
        }

        public void Deactivate()
        {
            m_GameStarted = false;
        }

        public override void Initialize()
        {
            Hide();
        }

        void Update()
        {
            if (Input.GetButtonDown("Pause"))
            {
                if (!m_GameStarted)
                {
                    Executive.OnGameStart?.Invoke();
                }

                SetPause(!m_GamePaused);
            }
        }

        public void ResumeGame()
        {
            SetPause(false);
        }

        private void SetPause(bool isPaused)
        {
            m_GamePaused = isPaused;

            this.SetEnabled(isPaused);
            Executive.SetPause(isPaused);
            Audio.AudioManager.SetPause(isPaused);

            if (isPaused)
            {
                Executive.OnGamePause?.Invoke();
            }
            else
            {
                Executive.OnGameResume?.Invoke();
            }
        }
    }
}
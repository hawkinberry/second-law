using UnityEngine;

namespace Utility.Menus
{
    public class StartMenu : BaseMenu
    {
        private void OnEnable()
        {
            // Register delegates
            Executive.OnGameStart += StartTransition;
        }

        private void OnDisable()
        {
            // Deregister delegates
            Executive.OnGameStart -= StartTransition;
        }

        private void Update()
        {
            if (IsVisible())
            {
                if (Input.GetButtonDown("Submit"))
                {
                    StartTransition();
                }
            }
        }
        public override void Initialize()
        {
            m_Overlay.enabled = true;
        }

        // Start is called before the first frame update
        public void StartTransition()
        {
            this.Hide();
        }

        public delegate void StartMenuHandler();
    }
}
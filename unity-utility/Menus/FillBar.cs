﻿using UnityEngine;
using UnityEngine.UI;

namespace Utility.Menus
{
    public class FillBar : MonoBehaviour
    {
        public Slider slider;

        public float Value
        {
            get { return slider.value; }
        }

        public void SetVal(float aVal)
        {
            slider.value = aVal;
        }

        public void Add(float aVal)
        {
            slider.value += aVal;
        }

        public void SetMax(float aVal)
        {
            slider.maxValue = aVal;
        }
    }
}
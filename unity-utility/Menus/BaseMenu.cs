using UnityEngine;

namespace Utility.Menus
{
    [RequireComponent(typeof(Canvas))]
    public abstract class BaseMenu : MonoBehaviour
    {
        protected Canvas m_Overlay;

        protected virtual void Start()
        {
            m_Overlay = GetComponent<Canvas>();
            Initialize();
        }

        public abstract void Initialize();

        public bool IsVisible()
        {
            return m_Overlay.enabled;
        }

        public void Enable()
        {
            gameObject.SetActive(true);
        }

        public void Disable()
        {
            gameObject.SetActive(false);
        }

        // Start is called before the first frame update
        public void Hide()
        {
            m_Overlay.enabled = false;
        }

        public void Show()
        {
            SetEnabled(true);
        }

        public void SetEnabled(bool setting)
        {
            m_Overlay.enabled = setting;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Consequences
{
    public class Storyline : MonoBehaviour
    {
        private HistoryScreen m_Parent;

        public GameObject cardTemplate;

        private List<StoryEvent> m_History;

        private static Storyline m_Instance;
        public static Storyline GetInstance()
        {
            return m_Instance;
        }

        private void Awake()
        {
            m_Parent = GetComponentInParent<HistoryScreen>();
        }

        private void Start()
        {
            if (!m_Instance)
            {
                m_Instance = this;
            }

            m_History = new List<StoryEvent>();

            // TMP
            //AddCard("Test 1", "Hello world.");
            //AddCard("Test 2", "Hello world.");
            //AddCard("Test 3", "Hello world.");
            //AddCard("Test 4", "Hello world.");
            //AddCard("Test 5", "Hello world.");
            //AddCard("Test 6", "Hello world.");
        }

        private void RemoveEmpty()
        {
            Transform example = transform.Find("Empty Card");
            if (example)
            {
                example.gameObject.SetActive(false);
            }
        }

        public static void Log(string title, string content)
        {
            if (!String.IsNullOrEmpty(title)) // don't create cards without titles
            {
                GetInstance().AddCard(title, content);
            }
        }

        private void AddCard(string title, string content)
        {
            // Remove empty card if this is the first time populating the list
            if (m_History.Count == 0)
            {
                RemoveEmpty();
            }

            // Search for an existing card to avoid duplicates
            Predicate<StoryEvent> t = (x => x.Title == title);
            Predicate<StoryEvent> c = (x => x.Content == content);
            Predicate<StoryEvent> d = x => (t(x) && c(x));
            if (m_History.FindAll(d).Count == 0)
            {
                StoryEvent card = Instantiate(cardTemplate, transform).GetComponent<StoryEvent>();
                card.name = title;
                card.SetTitle(title);
                card.SetContent(content);

                m_History.Add(card);
                m_Parent.SetNew(true);
            }
        }
    }
}
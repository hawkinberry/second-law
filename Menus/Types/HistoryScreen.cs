using UnityEngine;
using TMPro;

namespace Consequences
{
    public class HistoryScreen : Utility.Menus.BaseMenu
    {
        public static HistoryScreenHandler OnHistoryOpen;
        public static HistoryScreenHandler OnHistoryClose;

        public float rightEdgeHalf = 30;
        public float rightEdgeFull = -370;

        [SerializeField] private GameObject m_Button;
        private SpriteRenderer buttonSprite;

        public Sprite m_openedSprite;
        public Sprite m_closedSprite;

        public GameObject newFlag;

        private void OnEnable()
        {
            Utility.Executive.OnGameStart += ShowButton;
            Utility.Executive.OnGamePause += HalfScreen;
            Utility.Executive.OnGamePause += Expand;
            Utility.Executive.OnGameResume += Collapse;
            Utility.Executive.OnGameResume += FullScreen;
        }

        private void OnDisable()
        {
            Utility.Executive.OnGameStart -= ShowButton;
            Utility.Executive.OnGamePause -= HalfScreen;
            Utility.Executive.OnGamePause -= Expand;
            Utility.Executive.OnGameResume -= Collapse;
            Utility.Executive.OnGameResume -= FullScreen;
        }

        private void Awake()
        {
            buttonSprite = m_Button.GetComponentInChildren<SpriteRenderer>();
        }

        protected override void Start()
        {
            base.Start();

            // Init no display until game starts
            HideButton();
            FullScreen();
        }

        public override void Initialize()
        {
            Collapse();
            SetNew(false);
        }

        public void ShowButton()
        {
            m_Button.SetActive(true);
        }

        public void HideButton()
        {
            m_Button.SetActive(false);
        }

        public void FullScreen()
        {
            RectTransform rect = GetComponent<RectTransform>();
            // Offset max is actually negative of what is displayed in inspector
            rect.offsetMax = new Vector2(-1 * rightEdgeFull, rect.offsetMax.y);
        }

        public void HalfScreen()
        {
            RectTransform rect = GetComponent<RectTransform>();
            // Offset max is actually negative of what is displayed in inspector
            rect.offsetMax = new Vector2(-1 * rightEdgeHalf, rect.offsetMax.y);
        }

        public void Toggle()
        {
            if (IsVisible())
            {
                Collapse();
            }
            else
            {
                Expand();
            }
        }

        public void Expand()
        {
            Show();
            SetButtonState();
            SetNew(false);

            OnHistoryOpen?.Invoke();
        }

        public void Collapse()
        {
            Hide();
            SetButtonState();

            OnHistoryClose?.Invoke();
        }

        private void SetButtonState()
        {
            buttonSprite.sprite = (IsVisible() ? m_openedSprite : m_closedSprite);
        }

        public void SetNew(bool show)
        {
            newFlag.SetActive(show);
        }

        public delegate void HistoryScreenHandler();
    }
}
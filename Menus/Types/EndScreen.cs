using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Utility.Menus
{
    public class EndScreen : BaseMenu
    {
        public override void Initialize()
        {
            m_Overlay.enabled = false;
        }

        private void OnEnable()
        {
            Executive.OnEndGame += Show;
        }

        private void OnDisable()
        {
            Executive.OnEndGame -= Show;
        }

        public void StartTransition()
        {
            this.Hide();
        }
    }
}
